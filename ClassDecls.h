#pragma once
#include "visitors/IPrintVisitor.h"
#include "ClassDecl.h"
#include <vector>

struct ClassDecls{
  std::vector<ClassDecl*> classDecls;
  ClassDecls(ClassDecl *klass){
    classDecls.push_back(klass);
  }
  
  ~ClassDecls(){
    for(ClassDecl *klass : classDecls){
      delete klass;
    }
  }

  ClassDecls* add(ClassDecl *klass);
  void accept(IPrintVisitor *visitor);
};
