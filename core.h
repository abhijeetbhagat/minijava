#pragma once

#include<string> //for the comparison operator to work

///////////////////////////////////////////////////////////////
struct Table{
	std::string id;
	int value;
	Table *tail;
	Table(std::string _id, const int& _value, Table *t) : id(_id), value(_value), tail(t){}

	int lookup(const std::string & _id){
		return _lookup(_id, this);
	}

private:
	int _lookup(const std::string & _id, Table* t){
		if(t->id == _id){
			return t->value;
		}
		else{
			return _lookup(_id, t->tail);
		}
	}
};

Table *global_t = nullptr;

///////////////////////////////////////////////////////////////

struct Stm {
	virtual ~Stm()=0;
};

Stm::~Stm(){}
struct Exp {
	virtual ~Exp() = 0;
	virtual int evaluate() = 0;
};

Exp::~Exp(){}

struct EseqExp : public Exp {
	Stm* stm; 
	Exp* exp;
	EseqExp(Stm* s, Exp* e) :stm(s), exp(e){}

	int evaluate(){
		return exp->evaluate();
	}
};

struct ExpList {
	virtual ~ExpList()=0;
	explicit ExpList(const int & c = 0) : _count(c){}
	virtual int get_count() const{
		return _count;
	}
protected:
	int _count;
};

ExpList::~ExpList(){}

struct CompoundStm : public Stm {
	Stm *stm1, *stm2;
	CompoundStm(Stm* s1, Stm* s2):stm1(s1), stm2(s2) {}
};

struct AssignStm : public Stm {
	std::string id; 
	Exp* exp;
	AssignStm(const std::string & i, Exp* e):id(i), exp(e) {
		if(global_t){
			global_t = new Table(id, exp->evaluate(), nullptr);
		}
		else{
			//global_t->add(new Table(id, exp->evaluate(), nullptr));
			global_t = new Table(id, exp->evaluate(), global_t); //insert the new id as the first element in the linked list
		}
	}
};

struct PrintStm : public Stm {
	ExpList* exps;
	PrintStm(ExpList* e) : exps(e) {}
};

struct PairExpList : public ExpList {
	Exp* head;  
	ExpList* tail;
	PairExpList(Exp* h, ExpList* t) : ExpList(0), head(h), tail(t){}
	int get_count() const{
		return 1 + tail->get_count();
	}
};

struct LastExpList : public ExpList {
	Exp* head;
	LastExpList(Exp* h) : ExpList(1), head(h){}  
};

struct IdExp : public Exp {
	std::string id;
	IdExp(const std::string & i) : id(i){}

	int evaluate(){
		return global_t->lookup(id);
	}
};

struct NumExp : public Exp {
	int num;
	NumExp(const int& n): num(n){}

	int evaluate(){
		return num;
	}
};

struct OpExp : public Exp {
	Exp* left;
	int oper;
	Exp* right;
	const static int Plus=1,Minus=2,Times=3,Div=4;
	OpExp(Exp* l, const int& o, Exp* r) : left(l), oper(o), right(r){}

	int evaluate(){
		switch(oper){
		case Plus:
			return left->evaluate() + right->evaluate();
			break;
		case Minus:
			return left->evaluate() - right->evaluate();
			break;
		case Times:
			return left->evaluate() * right->evaluate();
			break;
		case Div:
			return left->evaluate() / right->evaluate();
			break;
		}
	}
};

const int OpExp::Plus;
const int OpExp::Minus;
const int OpExp::Times;
const int OpExp::Div;

