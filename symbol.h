#pragma once

class Symbol{
public:
  Symbol(std::string s) : sym(s){}

  std::string get_sym_name(){
    return sym;
  }
private:
  std::string sym;
};
