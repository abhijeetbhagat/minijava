#pragma once
#include "symbol.h"
#include "visitors/IPrintVisitor.h"


struct ClassDecl{
  Symbol *id;
  ClassDecl(Symbol* _id) : id(_id) {}

  void accept(IPrintVisitor *visitor);
};
