#pragma once
#include "../MainClass.h"
#include "../ClassDecl.h"

struct IPrintVisitor{
  virtual ~IPrintVisitor() = 0;
  virtual void visit(MainClass*) = 0;
  virtual void visit(ClassDecl*) = 0;
};
