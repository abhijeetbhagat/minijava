#pragma once

#include "IPrintVisitor.h"
#include "../MainClass.h"
#include "../ClassDecl.h"

struct PrettyPrintVisitor : public IPrintVisitor{
void visit(MainClass *mainClass);
void visit(ClassDecl *classDecl);
};
