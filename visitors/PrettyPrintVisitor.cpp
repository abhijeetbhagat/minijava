#include <iostream>
#include "PrettyPrintVisitor.h"

void PrettyPrintVisitor::visit(MainClass *mainClass){
  std::cout << mainClass->id->get_sym_name();
}

void PrettyPrintVisitor::visit(ClassDecl *classDecl){
  std::cout << classDecl->id->get_sym_name();
}


