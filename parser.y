%{
#include <iostream>
#include <cstdio>
#include "symbol.h"
#include "MainClass.h"
#include "ClassDecl.h"
#include "ClassDecls.h"
#include "Program.h"
#include "visitors/PrettyPrintVisitor.h"
  
#include <cassert>

using namespace std;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
 extern int yylineno;
 extern char *yytext;

void yyerror(const char *s); 
 
 
 Program *root;
%}

%locations

%union {
  string *str;
  Symbol *sym;
  Program *program;
  ClassDecl *classDecl;
  MainClass *mainClass;
  ClassDecls *classDecls;
}

%token <str> STRING
%token <str> PROPERTY_NAME
%token <str> CODE
%token <str> STRAY
 //%token <expr> INT
%token <sym> ID

%token INTEGER_LITERAL
%token CLASS
%token MAIN_FN
%token EXTENDS
%token PUBLIC
%token RETURN
%token INT  
%token BOOLEAN
%token IF    
%token ELSE
%token WHILE
%token PRINT 
%token LENGTH
%token TRUE 
%token FALSE
%token THIS  
%token NEW   
%token NEW_INT

%token PLUS
%token MINUS
%token MUL
%token ';'
%token LESS_THAN
%token ','
%token '='
%token '('
%token ')'
%token '{'
%token '}'
%token '['
%token ']'
%token '!'


%type <program>      Program
%type <mainClass>    MainClass
%type <classDecls>   ClassDecls
%type <classDecl>    ClassDecl

%left PLUS MINUS
%left MUL DIVIDE

%start Program
%%

Program: /*empty*/ {cout << "Nothing to parse\n";}
| MainClass ClassDecls {
  /* assert($3 != nullptr); */
  $$ = new Program($1, $2);
  root = $$;
}
;

MainClass: CLASS ID '{' MAIN_FN ID ')' '{' /* Statements */ '}' '}' {
  $$ = new MainClass($2);
}
;

ClassDecls : ClassDecl {$$ = new ClassDecls($1);}
| ClassDecls ClassDecl {$$ = $1->add($2);}
;

ClassDecl : /*empty*/ {}
| CLASS ID '{' '}' {$$ = new ClassDecl($2);}
| CLASS ID EXTENDS ID '{' '}' {$$ = new ClassDecl($2);}
;

/* VarDecls : VarDecl {} */
/* | VarDecls VarDecl {} */
/* ; */

/* VarDecl : Type ID ';' {} */
/* ; */

/* MethodDecls : MethodDecl {} */
/* | MethodDecls MethodDecl {} */
/* ; */

/* MethodDecl : PUBLIC Type ID '(' FormalList ')' '{' VarDecl Statement RETURN Exp ';' '}' {} */
/* ; */

/* FormalList : /\*empty*\/{} */
/* | Type ID {} */
/* | FormalList ',' Type ID {} */
/* ; */

/* Type : INT '['']' {} */
/* | BOOLEAN {} */
/* | INT {} */
/* | ID {} */
/* ; */

/* Statements : Statement {} */
/* | Statements Statement {} */
/* ; */

/* Statement : /\*empty*\/ {} */
/* | '{' Statement '}' {} */
/* | IF '(' Exp ')' Statement ELSE Statement {} */
/* | WHILE '(' Exp ')' Statement {} */
/* | PRINT '(' Exp ')' ';' {} */
/* | ID '=' Exp ';' {} */
/* | ID '[' Exp ']' '=' Exp ';' {} */
/* ; */

/* Exp : Exp PLUS Exp {} */
/* | Exp MINUS Exp {} */
/* | Exp MUL Exp {} */
/* | Exp LESS_THAN Exp {} */
/* | Exp '.' LENGTH {} */
/* | Exp '.' ID {} */
/* | INTEGER_LITERAL {} */
/* | TRUE {} */
/* | FALSE {} */
/* | ID {} */
/* | THIS {} */
/* | NEW_INT {} */
/* | NEW ID {} */
/* | '!' Exp {} */
/* | '(' Exp ')' {} */
/* ; */

/* ExpList : /\*empty*\/ {} */
/* | Exp {} */
/* | ExpList ',' Exp {} */
/* ; */
%%

int main(int argc, char *argv[]) {
                // open a file handle to a particular file:
  if(argc < 2){
    cout << "no input file specified.";
    exit(-1);
  }
                FILE *myfile = fopen(argv[1], "r");
                // make sure it is valid:
                if (!myfile) {
                 cout << "can't open the input file." << endl;
                 return -1;
                }
                // set flex to read from it instead of defaulting to STDIN:
                yyin = myfile;




                try{
                // parse through the input until there is no more:
                do {
                  yyparse();
                } while (!feof(yyin));
                }catch(std::exception e){}
}

void yyerror(const char *s) {
                cout << "Error: " << yylloc.first_line - 1 << "  " << yylloc.first_column << ' ' << yylloc.last_line << endl;
                // might as well halt now:
                exit(-1);
}

int yywrap() {return 1; }
