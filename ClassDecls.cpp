#include "ClassDecls.h"

ClassDecls* ClassDecls::add(ClassDecl *klass){
  classDecls.push_back(klass);
  return this;
}

void ClassDecls::accept(IPrintVisitor *visitor){
  for(auto * klass : classDecls){
    klass->accept(visitor);
  }
}
