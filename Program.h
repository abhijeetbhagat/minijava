#pragma once
#include "visitors/IPrintVisitor.h"
#include "MainClass.h"
#include "ClassDecls.h"

struct Program{
  MainClass *mainClass;
  ClassDecls *classDecls;
  
  Program(MainClass *m, ClassDecls *c) : mainClass(m), classDecls(c){}

  void accept(IPrintVisitor *visitor);
};

