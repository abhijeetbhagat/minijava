%{
#include <iostream>
//#include "expression.h"
#include "symbol.h"
using namespace std;
#define YY_DECL extern "C" int yylex()
#include "parser.tab.h"
 int yycolumn = 1;
#define YY_USER_ACTION {yylloc.first_line = yylineno; \
               yylloc.last_line = yylineno; \
               yylloc.first_column = yycolumn; yylloc.last_column = yycolumn + yyleng - 1;\
               yycolumn += yyleng; \
                }
%}

%option yylineno

Letter   [A-Za-z]
Digit    [0-9]
NameChar {Letter}|{Digit}|[-._:]
Name     ({Letter}|[_:]){NameChar}*

%%
[ \t]+             ;
\n                 {yycolumn = 1;}

"class"                                           return CLASS;
"public static void main(String []"               return MAIN_FN;
"extends"                                         return EXTENDS;
"public"                                          return PUBLIC;
"return"                                          return RETURN;
"int"                                             return INT;
"boolean"                                         return BOOLEAN;
"if"                                              return IF;
"else"                                            return ELSE;
"while"                                           return WHILE;
"System.out.println"                              return PRINT;
"length"                                          return LENGTH;
"true"                                            return TRUE;
"false"                                           return FALSE;
"this"                                            return THIS;
"new"                                             return NEW;
"new int"                                         return NEW_INT;

"+"                return PLUS;
"-"                return MINUS;
"*"                return MUL;
";"                return ';';
"<"                return LESS_THAN;
","                return ',';
"="                return '=';
"("                return '(';
")"                return ')';
"{"                return '{';
"}"                return '}';
"["                return '[';
"]"                return ']';
"!"                return '!';

[a-zA-Z]+          {yylval.sym = new Symbol(yytext); return ID;}
[0-9]+             {yylval.sym = new Symbol(yytext); return INTEGER_LITERAL;}
%%

int yywrap() { return 1; }
