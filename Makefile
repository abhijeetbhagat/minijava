
override CFLAGS += -Wall -Wextra  -pedantic

parser.tab.c parser.tab.h: parser.y
	win_bison -d parser.y

lex.yy.c: lexer.l parser.tab.h
	win_flex lexer.l

mjc: lex.yy.c parser.tab.c parser.tab.h symbol.h
	g++ -static-libgcc -static-libstdc++ -std=gnu++0x parser.tab.c lex.yy.c $(CFLAGS) -o mjc
