#pragma once
#include "symbol.h"
#include "visitors/IPrintVisitor.h"

struct MainClass{
  Symbol *id;
  MainClass(Symbol* _id) : id(_id) {}

  void accept(IPrintVisitor *visitor);
};
